# Wifi tests

We use the RPi4 onboard Broadcom/Cypress wifi chipset.

The full story is long, hesitant and full of twists and turns.
This is an attempt to summarize the key points in our context.

## Drivers

Note: Broadcom was acquired by Cypress in 2016, and even now,
the internet forums/issues/blogs is still confused about it.
Let's just acknowledge that nowadays, the `bcrm*` drivers are now
symlinks to the `cypress*` files - that makes it explicit, isn't it?

### Birth of an alternative yet upstream driver

2020: BSF and Kiwix investigate the matter and find that
some old RPi upstream driver shows the best results.

2021: [iiab/iiab#2853](https://github.com/iiab/iiab/issues/2853):
a RPi guy ask IIAB their opinion about what's wanted/expected/needed.
Turns out the IIAB context (and many many others, Kiwix & BSF included)
prefer more users over features.

2023: [raspberrypi/linux#3010](https://github.com/raspberrypi/linux/issues/3010)
quite reflects the years of discussions and constant ongoing changes adoption
by regular people:

- Broadcom chips (most if not all) are business driven proprietary crap,
  yet it is what powers the RPI hardware, deal with it
- the same RPI guy from `iiab/iiab#2853` decides to provide in parallel an optimized-as-possible
  driver for people prioritizing more clients.

A few months later, @Bastien-BSF investigates the issue and comes to the same conclusion:
we should use this old driver (spoiler: we do, since Florian/Kiwix'investigation).
He also explains the `raspberrypi/linux#3010` dilemma [with slides](bibliosansfrontieres/olip/olip-migration-project#15),
then writes a bunch of [python code](https://gitlab.com/bibliosansfrontieres/olip/devices/whyphy)
in order to benchmark the driver we are using.
Unfortunately I'm not able to find whether the results were noted,
nor if the `minimal` driver (in a PoC state at this point) was benchmarked.

2023 Q3: the RPi guy's work is merged after many many popcorn discussions.
As a result, the RPi distro ships two drivers:

- the `standard` one with bells and whistles, accepts 7 clients
- the `minimal` driver has many features stripped out,
  freeing enough RAM to uprise the clients amount to 19

It's been made clear this is the maximum to be expected
and no further optimization shall occur at this point.

### Minimal driver

The `minimal` driver is
([quoting](https://github.com/RPi-Distro/firmware-nonfree/tree/bullseye/debian/config/brcm80211/cypress))

> an alternative firmware that has been
> tuned to maximise the number of clients in AP mode while still supporting STA
> mode. The expected number of supported clients using this firmware is now 19.
> To achieve this, a number of features have been removed:
>
> - advanced roaming features (802.11k, 802.11v and 802.11r)
> - dfsradar - allows an AP to operate in channels that may be used by radar
>   systems
> - obss-obssdump - ACS (Auto Channel Support)
> - swdiv - antenna diversity (this is not relevant with only one antenna)

ACS is the only feature [we'd have an use for](bibliosansfrontieres/olip/olip-deploy#42)
but we can live without it.

Switching drivers is now as easy as using Debian's Alternatives mechanism:

```shell
update-alternatives --config cyfmac43455-sdio.bin
```

A quick test « à l'arrache » shows that 16 clients could connect
(we did not have more clients to use at this moment).

## Configuration

Involved:

- `hostapd.conf`
- FIXME dig OLIPv1-based investigations and tryouts

## Hardware wifi

When the `minimal` driver was merged, it's been suddenly been made clear - and after
years, official! that the only way to host more clients is to use an external
wifi USB dongle.

As a matter of fact, we recently tried « à l'arrache » to use a USB wifi dongle
scraped from a Koombook (Ralink RT5370 running `rt2870.bin`),
and without any other changes than `s/wlan0/wlan1/`, we could get 24 clients connected
(which does not equals 24 OLIP users, remember, we're talking about wifi only here).

When the time will come to pick a wifi USB dongle,
[morrownr/USB-WiFi](https://github.com/morrownr/USB-WiFi) should be quite helpful.

## Additional notes

The [iiab/iiab#2853](https://github.com/iiab/iiab/issues/2853), [raspberrypi/linux#3010](https://github.com/raspberrypi/linux/issues/3010)
issues and most of [morrownr/USB-WiFi](https://github.com/morrownr/USB-WiFi)
are gold mines worth a walk there. Among a wide variety configuration tips and
users experience reports, it paints an overview of the historical status quo and
progress around such concerns.
