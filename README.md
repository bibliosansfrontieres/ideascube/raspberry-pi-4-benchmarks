# Raspberry Pi 4 Benchmarks

This project is used to group informations about RPI 4 benchmarks.

- [CPU stress test](./stress-test/README.md)
- [Power consumption](./power-consumption/README.md)
- [Wifi tests](./wifi/README.md) (WIP)
