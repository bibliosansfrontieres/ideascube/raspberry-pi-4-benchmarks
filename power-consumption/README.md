# Raspberry Pi 4 power consumption

## Informations

Every tests are realized with a multimeter connected between Raspberry Pi 4 and alimentation.

All measurements are done in 30mn and multiplied times 2 for getting Wh.

## Measurements

### Idle

| No fan | 5V fan | 3V fan |
|-|-|-|
| 2Wh | 2.4Wh | 2.2Wh |

### 100% CPU load

| No fan | 5V fan | 3V fan |
|-|-|-|
| 5Wh | 5Wh | 5Wh |

## Battery duration

### Theory

A Pi Sugar 3 Plus should deliver **18.5Wh** in theory.

So a Raspberry that use **2Wh** should run for **9.25 hours**.

I made run the Raspberry "idle" untill battery is empty.

The Raspberry runned for **8.55 hours**.

With a simple calculation : 8.55 * 100 / 9.25 = 92.43

We know that the battery has 92.43% rendment (so ~8% energy loss).

We can safely say that the real battery power delivery is **17Wh**.

### Idle durations

| No fan | 5V fan | 3V fan |
|-|-|-|
| 8.55h | 7.08h | 7.72h |

### 100% CPU load durations

| No fan | 5V fan | 3V fan |
|-|-|-|
| 3.4h | 3.4h | 3.4h |
