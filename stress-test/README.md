# Raspberry Pi 4 stress tests

## Install stress test

```
sudo apt install stress python3-pip
pip3 install stressberry --user
pip3 install --upgrade numpy
```

### Run stress test

```
~/.local/bin/stressberry-run -n "Name" -d 1800 -i 300 -c 4 name.dat
```

`-d 1800` for 30 minutes stress test

`-i 300` for 5 minutes pause before and after stress test

`-c 4` for 4 CPU cores

### Graphic image of results

```
MPLBACKEND=Agg ~/.local/bin/stressberry-plot name.dat -f -d 300 -f -l 400 1850 -t 30 90 -o name.png --not-transparent
```

## Stress test without fan

### Conditions

- Raspberry Pi OS installed (bullseye x64)
- PiSugar 3 Plus connected to Raspberry Pi (making more heat inside case)
- 3D printed case [V3.0.0](https://gitlab.com/bibliosansfrontieres/ideascube/case)
- [GeeekPi 3510 Heatsink](https://www.amazon.fr/GeeekPi-Refroidissement-Dissipateur-Thermique-Ventilateur/dp/B07JGNF5F8/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=YHSWQVC2ND39&keywords=ventilateur+rpi+4&qid=1694185312&s=electronics&sprefix=ventilateur+rpi+4%2Celectronics%2C57&sr=1-3)
- Heatsink fan **NOT CONNECTED** to Raspberry Pi
- ~31°C ambient temperature

### Results

Orange : CPU frequency

Blue : CPU temperature

![Stress test no fan results](./stress_no_fan.png "Stress test no fan results")

### Conclusion

With 100% CPU load, CPU temperatures goes up to more than 80°C after 18 minutes and CPU frequency goes lower than 1.8GHz (lowest seen is 1.5GHz) after 80°C CPU temperature is reached.

## Stress test with MAX (5v) fan

### Conditions

- Raspberry Pi OS installed (bullseye x64)
- PiSugar 3 Plus connected to Raspberry Pi (making more heat inside case)
- 3D printed case [V3.0.0](https://gitlab.com/bibliosansfrontieres/ideascube/case)
- [GeeekPi 3510 Heatsink](https://www.amazon.fr/GeeekPi-Refroidissement-Dissipateur-Thermique-Ventilateur/dp/B07JGNF5F8/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=YHSWQVC2ND39&keywords=ventilateur+rpi+4&qid=1694185312&s=electronics&sprefix=ventilateur+rpi+4%2Celectronics%2C57&sr=1-3)
- Heatsink fan **CONNECTED** to Raspberry Pi (connected to 5v for max fan power)
- ~31°C ambient temperature

### Results

Orange : CPU frequency

Blue : CPU temperature

![Stress test with fan results](./stress_with_fan.png "Stress test with fan results")

### Conclusion

With 100% CPU load, CPU temperatures never goes up to more than 55°C and CPU frequency stays at 1.8GHz.


## Stress test with LOW (3v) fan

### Conditions

- Raspberry Pi OS installed (bullseye x64)
- PiSugar 3 Plus connected to Raspberry Pi (making more heat inside case)
- 3D printed case [V3.0.0](https://gitlab.com/bibliosansfrontieres/ideascube/case)
- [GeeekPi 3510 Heatsink](https://www.amazon.fr/GeeekPi-Refroidissement-Dissipateur-Thermique-Ventilateur/dp/B07JGNF5F8/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=YHSWQVC2ND39&keywords=ventilateur+rpi+4&qid=1694185312&s=electronics&sprefix=ventilateur+rpi+4%2Celectronics%2C57&sr=1-3)
- Heatsink fan **CONNECTED** to Raspberry Pi (connected to 3v for low fan power)
- ~31°C ambient temperature

### Results

Orange : CPU frequency

Blue : CPU temperature

![Stress test with low fan results](./stress_low_fan.png "Stress test with low fan results")

### Conclusion

With 100% CPU load, CPU temperatures never goes up to more than 58°C and CPU frequency stays at 1.8GHz.

